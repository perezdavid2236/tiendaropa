$(function() {
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		$(".carousel").carousel({
    			'interval':2500
    		});

    		$('#registrarse').on('hidden.bs.modal', function (e) {
			  console.log("Ya se ha ocultado el modal de Registro");
			  $("#btnRegistro").prop("disabled", false);
			  $("#btnRegistro").removeClass("btn-warning");
			  $("#btnRegistro").addClass("btn-success");
			});
			$('#registrarse').on('hide.bs.modal', function (e) {
			  console.log("Ocultando el modal de Registro");
			});
			$('#registrarse').on('shown.bs.modal', function (e) {
			  console.log("Ya se ha cargado el modal de Registro");
			  $("#btnRegistro").prop("disabled", true);
			  $("#btnRegistro").removeClass("btn-success");
			  $("#btnRegistro").addClass("btn-warning");
			});
			$('#registrarse').on('show.bs.modal', function (e) {
			  console.log("Mostrando el modal de Registro");
			});
});